azjakuma_missiontree_1_pt_1 = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	#conquer Hulibao
	Y01_harimari_fort = {
		icon = mission_build_up_to_force_limit
		position = 3
		required_missions = { Y01_ajgriijarul_training }
		provinces_to_highlight = {
			area = moryokang_area
			NOT = {
				owned_by = ROOT
			}
		}

		trigger = {
			owns_all_provinces = {
				area = moryokang_area
			}
		}
		effect = {
			4901 = {
				add_province_modifier = {
					name = azjakuma_southern_bulwark
					duration = 18250 #50 years
				}
			}
		}
	}

	Y01_expel_rebellious = {
		icon = mission_build_up_to_force_limit
		position = 5
		required_missions = { Y01_harimari_fort }
		provinces_to_highlight = {
			area = moryokang_area
			NOT = {
				OR = {
					culture = hill_yan
					culture = horned_ogre
				}
				religion = lefthand_path
			}
		}

		trigger = {
			moryokang_area = {
				owned_by = Y01
				has_owner_religion = yes
				OR = {
					culture = horned_ogre
					culture = hill_yan
				}
				type = all
			}
		}
		effect = {
			moryokang_area = {
				limit = {
					nationalism = 1
				}
				add_nationalism = -5
			}
		}
	}

	Y01_direct_control_tribute = {
		icon = mission_build_up_to_force_limit
		position = 6
		required_missions = { Y01_expel_rebellious }
		provinces_to_highlight = {
			province_id = 4813
		}

		trigger = {
			if = {
				limit = {
					Y89 = {
						exists = yes
					}
					OR = {
						Y01 = {
							is_subject_of_type_with_overlord = {
							who = Y89
							type = tributary_state
							}
						}
						is_subject = no
					}
				}
				Y89 = {
					has_opinion = {
						who = ROOT
						value = 100
					}
					has_spy_network_from = {
						who = ROOT
						value = 20
					}
				}
			}
			else = {
				dip_power = 20
			}
		}
		effect = {
			if = {
				limit = {
					Y89 = {
						exists = yes
					}
					OR = {
						Y01 = {
							is_subject_of_type_with_overlord = {
							who = Y89
							type = tributary_state
							}
						}
						is_subject = no
					}
				}
				country_event = {
					id = flavor_azjakuma.26
				}
			}
			else = {
				mogutian_area = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
		}
	}

	Y01_integration_yuantsai = {
		icon = mission_build_up_to_force_limit
		position = 7
		required_missions = { Y01_direct_control_tribute }
		provinces_to_highlight = {
			area = mogutian_area
			NOT = {
				is_core = ROOT
				owned_by = ROOT
			}
		}

		trigger = {
			owns_core_province = 4813
			owns_core_province = 4814
		}
		effect = {
			add_country_modifier = {
				name = azjakuma_accpeted_yuantsai #gives additional accepted culture slot
				duration = -1
			}
			add_accepted_culture = western_yan
		}
	}

	Y01_expand_record_keeping = {
		icon = mission_build_up_to_force_limit
		position = 8
		required_missions = { Y01_integration_yuantsai }
		provinces_to_highlight = {
			province_id = 4813
		}

		trigger = {
			4813 = {
				owned_by = ROOT
				has_production_building_trigger = yes
				base_production = 10
			}
		}
		effect = {
			add_country_modifier = {
				name = azjakuma_expanded_record_keeping
				duration = 3650 #10 years
			}
		}
	}
}

azjakuma_missiontree_2_pt_1 = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	Y01_truce_offer = {
		icon = mission_build_up_to_force_limit
		position = 1
		required_missions = { }
		provinces_to_highlight = { }

		trigger = {
			dip_power = 50
		}
		effect = {
			add_dip_power = -50
			country_event = {
				id = flavor_azjakuma.15
			}
		}
	}

	Y01_ajgriijarul_training = {
		icon = mission_oni
		position = 2
		required_missions = { Y01_truce_offer }
		provinces_to_highlight = {
			province_id = 4829
		}

		trigger = {
			4829 = {
				development = 15
			}
			faction_in_power = oni_bright_claw
		}
		effect = {
			add_country_modifier = {
				name = azjakuma_bright_claw_warriors
				duration = 3650 #10 years
			}
		}
	}

	Y01_chumijemoya_spymasters = {
		icon = mission_build_up_to_force_limit
		position = 4
		required_missions = { Y01_harimari_fort Y01_bring_birb_to_heel }
		provinces_to_highlight = {
			province_id = 4831
		}

		trigger = {
			4831 = {
				development = 12
			}
			OR = { #TODO add random events for advisors from factions
				spymaster = 1
				faction_in_power = oni_silent_mist
			}
		}
		effect = {
			add_faction_influence = {
				faction = oni_silent_mist
				influence = 20
			}
			add_country_modifier = {
				name = azjakuma_silent_mist_spymasters
				duration = 3650 #10 years
			}
		}
	}

	Y01_infiltrate_eunuch_council = {
		icon = mission_build_up_to_force_limit
		position = 5
		required_missions = { Y01_chumijemoya_spymasters }
		provinces_to_highlight = { }

		trigger = {
			OR = {
				AND = {
					4848 = {
						owner = {
							has_spy_network_from = {
								who = ROOT
								value = 40
							}
						}
					}
					OR = {
						NOT = {
							has_ruler = "Tiraga Aromo"
						}
						faction_in_power = oni_silent_mist
					}
				}
				owns = 4848
			}
		}
		effect = {
			jinqiu_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			if = {
				limit = {
					4848 = {
						NOT = { owned_by = ROOT }
					}
				}
				tooltip = {
					4848 = {
						owner = {
							add_country_modifier = {
								name = azjakuma_infiltrated_khelorvalshi
								duration = 3650
							}
						}
					}
				}
				hidden_effect = {
					4848 = {
						owner = {
							country_event = {
								id = flavor_azjakuma.25
							}
						}
					}
				}
			}
		}
	}

	Y01_cursed_souls_khelorvalshi = {
		icon = mission_build_up_to_force_limit
		position = 6
		required_missions = { Y01_expel_rebellious Y01_infiltrate_eunuch_council }
		provinces_to_highlight = {
			province_id = 4848
		}
		
		trigger = {
			owns_core_province = 4848
		}
		effect = {
			4848 = {
				change_culture = horned_ogre
				change_religion = ROOT
				rename_capital = "Khelorvalshi"
				change_province_name = "Khelorvalshi"
			}
			country_event = {
				id = flavor_azjakuma.30
			}
		}
	}

	Y01_restoring_khelorvalshi = {
		icon = mission_build_up_to_force_limit
		position = 7
		required_missions = { Y01_direct_control_tribute Y01_cursed_souls_khelorvalshi }
		provinces_to_highlight = {
			province_id = 4848
		}
		
		trigger = {
			owns_core_province = 4848
			4848 = {
				development = 12
			}
		}
		effect = {
			4848 = {
				add_permanent_province_modifier = {
					name = azjakuma_shirgrii
					duration = -1
				}
			}
		}
	}

	Y01_khelorvalshi_libraries = {
		icon = mission_build_up_to_force_limit
		position = 9
		required_missions = { Y01_expand_record_keeping Y01_restoring_khelorvalshi }
		provinces_to_highlight = { }

		trigger = {
			owns_core_province = 4848
			adm_power = 100
			dip_power = 100
			mil_power = 100
		}
		effect = {
			add_adm_power = -100
			add_dip_power = -100
			add_mil_power = -100
			country_event = {
				id = flavor_azjakuma.31
			}
		}
	}

	Y01_way_forward = {
		icon = mission_build_up_to_force_limit
		position = 10
		required_missions = { Y01_khelorvalshi_libraries Y01_birb_auxillaries }
		provinces_to_highlight = { }

		trigger = {
			stability = 2
			OR = {
				has_reform = internal_mission_reform
				has_reform = external_mission_reform
				has_reform = mission_to_civilize_reform
			}
		}
		effect = {
			country_event = {
				id = flavor_azjakuma.32
			}
		}
	}
}

azjakuma_missiontree_3_pt_1 = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	Y01_bring_birb_to_heel = {
		icon = mission_build_up_to_force_limit
		position = 3
		required_missions = { Y01_ajgriijarul_training }
		provinces_to_highlight = {
			OR = {
				area = choogiaza_area
				area = yunghuun_area
				province_id = 5412
				province_id = 5415
			}
			NOT = {
				owned_by = ROOT
			}
		}

		trigger = {
			custom_trigger_tooltip = {
				tooltip = azjakuma_bring_birb_to_heel_tooltip
				choogiaza_area = {
					type = all
					country_or_non_sovereign_subject_holds = ROOT
				}
				yunghuun_area = {
					type = all
					country_or_non_sovereign_subject_holds = ROOT
				}
				5412 = {
					country_or_non_sovereign_subject_holds = ROOT
				}
				5415 = {
					country_or_non_sovereign_subject_holds = ROOT
				}
			}
		}
		effect = {
			add_stability = 1
			hidden_effect = {
				country_event = {
					id = flavor_azjakuma.103
					days = 3650
				}
			}
		}
	}

	Y01_winning_over_birb = {
		icon = mission_build_up_to_force_limit
		position = 5
		required_missions = { Y01_bring_birb_to_heel }
		provinces_to_highlight = { }

		trigger = {
			custom_trigger_tooltip = {
				tooltip = azjakuma_birb_conquered_decade_tooltip
				has_country_flag = birb_conquered_decade
			}
			accepted_culture = shuvuush
		}

		effect = {
			country_event = {
				id = flavor_azjakuma.21
			}
			hidden_effect = {
				clr_country_flag = oni_birb_rejection_setup
				set_country_flag = oni_birb_caution_setup
				set_country_flag = oni_birb_acceptance_possible
			}
		}
	}
	
	Y01_birb_auxillaries = {
		icon = mission_build_up_to_force_limit
		position = 9
		required_missions = { Y01_winning_over_birb Y01_expand_iron_mines }
		provinces_to_highlight = {
			region = shuvuushudi_region
			trade_goods = grain
			country_or_non_sovereign_subject_holds = ROOT
			NOT = {
				base_manpower = 3
			}
		}

		trigger = { #unless someone figures out how to add the base manpower of all shuvuush grain provinces I will switch to this trigger
			#add in if for trade good change
			num_of_owned_provinces_with = {
				value = 9
				AND = {
					region = shuvuushudi_region
					trade_goods = grain
					base_manpower = 3
				}
			} 
		}
		effect = {
			add_country_modifier = {
				name = azjakuma_birb_auxillaries
				duration = 18250 #50 years
			}
		}
	}
}

azjakuma_missiontree_4_pt_1 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	Y01_breadbasket_north = {
		icon = mission_build_up_to_force_limit
		position = 4
		required_missions = { Y01_bring_birb_to_heel }
		provinces_to_highlight = { }

		trigger = {
			years_of_income = 1.5
			adm_power = 100
			dip_power = 50
		}
		effect = {
			add_years_of_income = -1.5
			add_adm_power = -100
			add_dip_power = -50
			custom_tooltip = azjakuma_breadbasket_north_tooltip
			hidden_effect = {
				every_owned_province = {
					limit = {
						region = shuvuushudi_region
						trade_goods = grain
					}
					add_base_production = 2
				}
				set_country_flag = oni_birb_immigration_allowed
			}
		}
	}

#	Y01_defend_demon_hills = {
#		icon = mission_build_up_to_force_limit
#		position = 6
#		required_missions = { Y01_defend_north }
#		provinces_to_highlight = { }
#
#		trigger = {
#			is_in_war = {
#				attacker_leader = R62
#				defender_leader = ROOT
#			}
#		}
#		effect = {
#			add_country_modifier = {
#				name = azjakuma_defend_command
#				duration = -1
#			}
#			custom_tooltip = azjakuma_defend_demon_hills_tooltip
#			hidden_effect = {
#				set_variable = {
#					which = azjakuma_defend_command_exhaustion_variable
#					value = 0
#				}
#				set_country_flag = oni_defensive_war
#				country_event = {
#					id = flavor_azjakuma.102
#					days = 365
#				}
#			}
#		}
#	}
}

azjakuma_missiontree_5_pt_1 = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	Y01_adapting_oni_state = {
		icon = mission_build_up_to_force_limit
		position = 1
		required_missions = { }
		provinces_to_highlight = { }

		trigger = {
			treasury = 200
		}
		effect = {
			add_treasury = -200
			hidden_effect = {
				every_owned_province = {
					limit = {
						region = demon_hills_region
						has_terrain = hills
					}
					add_province_modifier = {
						name = azjakuma_expanding_infrastructure
						duration = 18250
					}
				}
			}
			custom_tooltip = azjakuma_adapting_oni_state_tooltip
		}
	}

	Y01_teimarji_spring = {
		icon = mission_build_up_to_force_limit
		position = 2
		required_missions = { Y01_adapting_oni_state }
		provinces_to_highlight = {
			province_id = 5430
		}

		trigger = {
			5430 ={
				development = 20
			}
		}
		effect = {
			add_country_modifier = {
				name = azjakuma_korashi_talismans #-20% shock damage recieved, +5% mages influence
				duration = 5475 #15 years
			}
		}
	}

	Y01_kabiurgarko_tributary = {
		icon = mission_build_up_to_force_limit
		position = 3
		required_missions = { Y01_teimarji_spring }
		provinces_to_highlight = {
			province_id = 4828
		}

		trigger = {
			4828 = {
				base_production = 8
			}
		}
		effect = {
			4828 = {
				add_province_modifier = {
					name = azjakuma_expanded_tributary_system
					duration = 9125
				}
			}
			add_faction_influence = {
				faction = oni_golden_gates
				influence = 10
			}
		}
	}

	Y01_golden_gates = {
		icon = mission_build_up_to_force_limit
		position = 4
		required_missions = { Y01_kabiurgarko_tributary }
		provinces_to_highlight = {
			province_id = 4828
		}

		trigger = {
			4828 = {
				has_trade_building_trigger = yes
			}
			faction_in_power = oni_golden_gates
		}
		effect = {
			4828 = {
				center_of_trade = 1
			}
			add_faction_influence = {
				faction = oni_golden_gates
				influence = 10
			}
		}
	}

	Y01_defend_north = {
		icon = mission_build_up_to_force_limit
		position = 5
		required_missions = { Y01_breadbasket_north Y01_golden_gates }

		trigger = {
			OR = {
				4973 = {
					fort_level = 2
				}
				4833 = {
					fort_level = 2
				}
			}
		}
		effect = {
			if = {
				limit = {
					4973 = {
						fort_level = 2
					}
				}
				4973 = {
					add_province_modifier = {
						name = azjakuma_defend_north
						duration = 9125
					}
				}
			}
			if = {
				limit = {
					4833 = {
						fort_level = 2
					}
				}
				4833 = {
					add_province_modifier = {
						name = azjakuma_defend_north
						duration = 9125
					}
				}
			}
		}
	}

	Y01_expand_iron_mines = {
		icon = mission_build_up_to_force_limit
		position = 8
		required_missions = { Y01_defend_north }
		trigger = {
			any_owned_province = {
				trade_goods = iron
				base_production = 8
			}
		}
		effect = {
			add_country_modifier = {
				name = azjakuma_expanded_iron_mines #-10% land maintenance
				duration = 5475 #15 years
			}
		}
	}
}

